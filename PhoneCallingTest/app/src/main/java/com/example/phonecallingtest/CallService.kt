package com.example.phonecallingtest

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telecom.Call
import android.telecom.CallScreeningService
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.phonecallingtest.network.ContactAPIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.log

class CallService : CallScreeningService() {
    companion object {
        val phoneNumberLiveData = MutableLiveData<String?>()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onScreenCall(p0: Call.Details) {
        val phoneNumber = p0.handle.schemeSpecificPart
        Log.d("CallService", "Ringing Number is - $phoneNumber")
        Toast.makeText(this, phoneNumber, Toast.LENGTH_SHORT).show()
        saveNumber(this, phoneNumber)
//        val token = loadLoginData(this)
//        getId(phoneNumber, token!!)


    }


    fun saveNumber(context: Context, number: String) {
        val sharedPrefrences = context.getSharedPreferences("number", Context.MODE_PRIVATE)
        with(sharedPrefrences.edit()) {
            putString("phone", number)
            apply()
        }
        phoneNumberLiveData.postValue(number)
        // TODO try to delete liveData
    }
}

