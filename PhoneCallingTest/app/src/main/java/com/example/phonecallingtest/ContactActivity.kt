package com.example.phonecallingtest

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.phonecallingtest.network.Contact
import com.example.phonecallingtest.network.ContactInfoAPIClient
import com.example.phonecallingtest.network.ContactInfoApi
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.Manifest
import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.example.phonecallingtest.network.CommentHistoryItem
import com.example.phonecallingtest.network.CommentHistoryResponse


class ContactActivity() : AppCompatActivity() {
    private lateinit var contactNameTextView: TextView
    private lateinit var contactLastNameTextView: TextView
    private lateinit var contactDirectionTextView: TextView
    private lateinit var contactScoreTextView: TextView
    private lateinit var contactPriorityTextView: TextView
    private lateinit var contactPhoneNumberTextView: TextView
    private lateinit var contactSnilsTextView: TextView
    private lateinit var parentPhoneNumberTextView: TextView
    private lateinit var contactCommentEditText: EditText
    private lateinit var BtnToSend: Button
    private lateinit var statusIcon: ImageView
    private lateinit var commentHistoryTextView: TextView
    private var commentHistory: List<CommentHistoryItem> = emptyList()
    private var newStatus: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        contactNameTextView = findViewById(R.id.contact_name)
        contactLastNameTextView = findViewById(R.id.contact_lastName)
        contactDirectionTextView = findViewById(R.id.direction)
        contactScoreTextView = findViewById(R.id.score)
        contactPriorityTextView = findViewById(R.id.priority)
        contactPhoneNumberTextView = findViewById(R.id.phone_number)
        parentPhoneNumberTextView = findViewById(R.id.parent_phone_number)
        contactSnilsTextView = findViewById(R.id.snils)
        contactCommentEditText = findViewById(R.id.comment_edit_text)
        BtnToSend = findViewById(R.id.sendBtn)
        statusIcon = findViewById(R.id.contactStatus)
        commentHistoryTextView = findViewById(R.id.commentHistoryTextView)

        fun loadLoginData(context: Context): Pair<String?, String?> {
            val sharedPreferences = context.getSharedPreferences("LoginData", MODE_PRIVATE)
            val authToken = sharedPreferences.getString("authToken", null)
            val dataTable = sharedPreferences.getString("dataTable", null)
            return Pair(authToken, dataTable)
        }

        if (ContextCompat.checkSelfPermission(
                this@ContactActivity,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@ContactActivity,
                arrayOf(Manifest.permission.CALL_PHONE), 101
            )
        }

        val call: ImageView = findViewById(R.id.phone_call_icon)
        val callParent: ImageView = findViewById(R.id.phone_call_icon2)
        call.setOnClickListener {
            val phoneNumber = contactPhoneNumberTextView.text.toString()
            val intent: Intent = Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null))
            startActivity(intent)
        }
        callParent.setOnClickListener {
            val phoneNumber = parentPhoneNumberTextView.text.toString()
            val intent: Intent = Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null))
            startActivity(intent)
        }

        BtnToSend.setOnClickListener {
            val (token, dataTable) = loadLoginData(this)
            val contact = intent.getSerializableExtra("contact") as Contact
            val id = contact.id

            val commentText = contactCommentEditText.text.toString()
            val updatedComment = JsonObject().apply {
                addProperty("comment", commentText)
            }

            ContactInfoAPIClient.contactReqInfo.updateComment(
                token!!,
                id,
                dataTable!!,
                updatedComment
            )
                .enqueue(object : Callback<Contact> {
                    override fun onResponse(call: Call<Contact>, response: Response<Contact>) {
                        if (response.isSuccessful) {
                            Toast.makeText(
                                applicationContext,
                                "Комментарий успешно обновлен", Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Log.e(
                                "ContactActivity",
                                "Response is not successful: ${response.code()}"
                            )
                            Toast.makeText(
                                applicationContext,
                                "Error updating comment", Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onFailure(call: Call<Contact>, t: Throwable) {
                        Log.e("ContactActivity", "Error updating comment: ${t.localizedMessage}")
                        Toast.makeText(
                            applicationContext,
                            "Не удалось обновить комментарий",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
        }

        val retrofit = Retrofit.Builder()
            .baseUrl("http://84.201.179.32/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val contactInfoApi = retrofit.create(ContactInfoApi::class.java)
        val contact = intent.getSerializableExtra("contact") as Contact
        val (token, dataTable) = loadLoginData(this)

        val id = contact.id

        contactInfoApi.getContactInfo(token!!, id, dataTable!!).enqueue(object : Callback<Contact> {
            override fun onResponse(call: Call<Contact>, response: Response<Contact>) {
                if (response.isSuccessful) {
                    val contact = response.body()
                    if (contact != null) {
                        populateContactFields(contact)
                    } else {
                        Log.e("ContactActivity", "Contact is null")
                    }
                } else {
                    Log.e("ContactActivity", "Response is not successful: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Contact>, t: Throwable) {
                Log.e("ContactActivity", "Error fetching contact: ${t.localizedMessage}")
            }
        })

        contactInfoApi.getCommentHistory(token!!, id, dataTable!!)
            .enqueue(object : Callback<CommentHistoryResponse> {
                override fun onResponse(
                    call: Call<CommentHistoryResponse>,
                    response: Response<CommentHistoryResponse>
                ) {
                    if (response.isSuccessful) {
                        val commentHistoryResponse = response.body()

                        if (commentHistoryResponse != null) {
                            populateCommentHistory(commentHistoryResponse.list)
                        } else {
                            Log.e("ContactActivity", "Comment history response is null")
                        }
                    } else {
                        Log.e(
                            "ContactActivity",
                            "Comment history response is not successful: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<CommentHistoryResponse>, t: Throwable) {
                    Log.e(
                        "ContactActivity",
                        "Error fetching comment history: ${t.localizedMessage}"
                    )
                }
            })

        commentHistoryTextView.setOnClickListener {
            showCommentHistoryDialog(commentHistory)
        }

        statusIcon.setOnClickListener {
            showStatusDialog(token!!, id, dataTable!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun finish() {
        super.finish()
    }

    private fun populateContactFields(contact: Contact) {
        contactNameTextView.text = contact.name
        contactLastNameTextView.text = contact.lastName
        contactDirectionTextView.text = "Направление: ${contact.speciality}"
        contactScoreTextView.text = "Баллы: ${contact.score}"
        contactPriorityTextView.text = "${contact.priority}"
        contactPhoneNumberTextView.text = "Номер телефона: ${contact.phoneNumber}"
        parentPhoneNumberTextView.text = "Номер родителя: ${contact.parentPhoneNumber}"
        contactSnilsTextView.text = "Снилс: ${contact.snils}"
        contactCommentEditText.setText(contact.comment)
        Log.d("ContactActivity", contact.toString())
        val statusImageResource = when (contact.status) {
            "Согласен" -> R.drawable.green_status
            "Думает" -> R.drawable.yellow_status
            "Отказ" -> R.drawable.red_status
            "Нет" -> R.drawable.clear_status
            else -> 1
        }
        statusIcon.setImageResource(statusImageResource)
    }

    private fun populateCommentHistory(commentHistoryList: List<CommentHistoryItem>) {
        commentHistory = commentHistoryList
        for (commentItem in commentHistory) {
            val commentHistoryData = "Comment: ${commentItem.data.attributes.became.comment}, " +
                    "Created by: ${commentItem.createdByName}, " +
                    "Created at: ${commentItem.getCreatedAt()}, " +
                    "Modified at: ${commentItem.getModifiedAt()}"

            Log.d("ContactActivity", "commentHistoryData: $commentHistoryData")
        }
    }

    private fun showCommentHistoryDialog(commentHistory: List<CommentHistoryItem>) {
        if (commentHistory.isEmpty()) {
            AlertDialog.Builder(this)
                .setTitle("История комментариев")
                .setMessage("История комментариев пуста.")
                .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                .show()
            return
        }

        val commentHistoryText = StringBuilder()
        for ((index, commentItem) in commentHistory.withIndex()) {
            commentHistoryText.append("Запись ${index + 1}:\n")
            if (commentItem.data.attributes.became.comment != null) {
                commentHistoryText.append("Комментарий: ${commentItem.data.attributes.became.comment}\n")
            } else {
                commentHistoryText.append("Комментарий: Не изменялся\n")
            }
            if (commentItem.data.attributes.became.status != null) {
                commentHistoryText.append("Статус: ${commentItem.data.attributes.became.status}\n")
            } else {
                commentHistoryText.append("Статус: Не изменялся\n")

            }
            commentHistoryText.append("Создано/изменено: ${commentItem.createdByName} - ${commentItem.getModifiedAt()}\n\n")
        }

        AlertDialog.Builder(this)
            .setTitle("История комментариев")
            .setMessage(commentHistoryText.toString())
            .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun showStatusDialog(token: String, id: String, dataTable: String) {
        val statusOptions = arrayOf("Думает", "Согласен", "Отказ", "Нет")
        var selectedStatusIndex = -1

        AlertDialog.Builder(this)
            .setTitle("Выберите статус")
            .setSingleChoiceItems(statusOptions, selectedStatusIndex) { dialog, which ->
                selectedStatusIndex = which
                newStatus = statusOptions[which]
            }
            .setPositiveButton("OK") { dialog, _ ->
                if (selectedStatusIndex != -1) {
                    updateStatusOnServer(
                        token,
                        id,
                        dataTable,
                        newStatus
                    ) // Update the status on the server
                }
                dialog.dismiss()
            }
            .setNegativeButton("Отмена") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun updateStatusOnServer(
        token: String,
        id: String,
        dataTable: String,
        newStatus: String
    ) {
        val updatedStatus = JsonObject().apply {
            addProperty("status", newStatus)
        }
        ContactInfoAPIClient.contactReqInfo.updateStatus(token, id, dataTable, updatedStatus)
            .enqueue(object : Callback<Contact> {
                override fun onResponse(call: Call<Contact>, response: Response<Contact>) {
                    if (response.isSuccessful) {
                        Toast.makeText(
                            applicationContext,
                            "Статус успешно обновлен", Toast.LENGTH_LONG
                        ).show()
                        refreshContactInfo(token, id, dataTable)
                        // Update UI or perform other actions after successful update
                    } else {
                        Log.e("ContactActivity", "Response is not successful: ${response.code()}")
                        Toast.makeText(
                            applicationContext,
                            "Error updating status", Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<Contact>, t: Throwable) {
                    Log.e("ContactActivity", "Error updating status: ${t.localizedMessage}")
                    Toast.makeText(
                        applicationContext,
                        "Не удалось обновить статус",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun refreshContactInfo(token: String, id: String, dataTable: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://84.201.179.32/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val contactInfoApi = retrofit.create(ContactInfoApi::class.java)
        contactInfoApi.getContactInfo(token, id, dataTable)
            .enqueue(object : Callback<Contact> {
                override fun onResponse(call: Call<Contact>, response: Response<Contact>) {
                    if (response.isSuccessful) {
                        val updatedContact = response.body()
                        if (updatedContact != null) {
                            populateContactFields(updatedContact) // Обновляем UI
                        } else {
                            Log.e("ContactActivity", "Contact is null")
                        }
                    } else {
                        Log.e("ContactActivity", "Response is not successful: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<Contact>, t: Throwable) {
                    Log.e("ContactActivity", "Error fetching contact: ${t.localizedMessage}")
                }
            })
    }
}