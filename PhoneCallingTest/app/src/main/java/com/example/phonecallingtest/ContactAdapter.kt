package com.example.phonecallingtest

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.phonecallingtest.network.Contact
import com.example.phonecallingtest.network.ContactAPIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.TimerTask


class ContactAdapter(private val contacts: List<Contact>, context: Context) :
    RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {
    val token = loadLoginData(context)
    private val sharedPreferences =
        context.getSharedPreferences("CommentData", Context.MODE_PRIVATE)
    private val viewedContacts = mutableSetOf<String>() // Track viewed contacts


    class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val contactName: TextView = itemView.findViewById(R.id.contact_name)
        val contactLastName: TextView = itemView.findViewById(R.id.contact_lastName)
        val contactDirection: TextView = itemView.findViewById(R.id.direction)
        val contactScore: TextView = itemView.findViewById(R.id.score)
        val contactPriority: TextView = itemView.findViewById(R.id.priority)
        val contactComment: TextView = itemView.findViewById(R.id.comment)
        val contactStatus: ImageView = itemView.findViewById(R.id.contactStatus)
        val indicatorView: View = itemView.findViewById(R.id.commentIndicator)


        fun bind(contact: Contact) {
            contactName.text = contact.name
            contactLastName.text = contact.lastName
            contactDirection.text = "Направление: ${contact.speciality}"
            contactScore.text = "Баллы: ${contact.score}"
            contactPriority.text = " ${contact.priority}"
            contactComment.text = "Комментарий: ${contact.comment}"


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = contacts[position]
        holder.bind(contact)
        val id = contact.id

        if (contact.status != null) {
            val statusImageResource = when (contact.status) {
                "Согласен" -> R.drawable.green_status
                "Думает" -> R.drawable.yellow_status
                "Отказ" -> R.drawable.red_status
                "Нет" -> R.drawable.clear_status
                else -> 1
            }
            holder.contactStatus.setImageResource(statusImageResource)
        } else {
            holder.contactStatus.setImageResource(R.drawable.clear_status)
        }


        val commentKey = "comment_${contact.id}" // Unique key for each contact's comment
        val previousComment = sharedPreferences.getString(commentKey, null)
        val currentComment = contact.comment
        Log.d(
            "ContactAdapter",
            "Current comment is " + currentComment + " " + "previous comment is " + previousComment
        )
        if (currentComment != null && currentComment != previousComment && !viewedContacts.contains(
                id
            )
        ) {
            holder.indicatorView.visibility = View.VISIBLE
            sharedPreferences.edit().putString(commentKey, currentComment).apply()
        } else {
            holder.indicatorView.visibility = View.INVISIBLE
        }

        holder.itemView.setOnClickListener {
            viewedContacts.add(id) // Mark contact as viewed
            notifyItemChanged(position)
            val intent = Intent(it.context, ContactActivity::class.java)
            intent.putExtra("contact", contact)
            Log.d("Test", "Clicked")
            Log.d("Test", "$contact")
            it.context.startActivity(intent)
        }
    }


    private fun loadLoginData(context: Context): String? {
        val sharedPreferences = context.getSharedPreferences(
            "LoginData",
            AppCompatActivity.MODE_PRIVATE
        )
        val authToken = sharedPreferences.getString("authToken", null)
        return authToken
    }

    override fun getItemCount() = contacts.size
}