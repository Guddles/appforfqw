package com.example.phonecallingtest

import android.app.Activity
import android.app.role.RoleManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.phonecallingtest.network.Contact
import com.example.phonecallingtest.network.ContactAPIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@RequiresApi(Build.VERSION_CODES.Q)
class MainActivity : AppCompatActivity() {
    private val roleManager by lazy { getSystemService(RoleManager::class.java) }
    private lateinit var recyclerView: RecyclerView
    private lateinit var spinner: Spinner
    private lateinit var statusSpinner: Spinner
    private var currentCategory = "Все"
    private var currentStatus = "Все"
    private val handler = Handler(Looper.getMainLooper())
    private var launchedFromNotification = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recycler_view)
        val searchButton = findViewById<ImageView>(R.id.search)
        val clearButton = findViewById<ImageView>(R.id.clear)
        var searchableEditText = findViewById<EditText>(R.id.searchableText)

        fun loadLoginData(context: Context): Pair<String?, String?> {
            val sharedPreferences = context.getSharedPreferences("LoginData", MODE_PRIVATE)
            val authToken = sharedPreferences.getString("authToken", null)
            val dataTable = sharedPreferences.getString("dataTable", null)
            val p = Pair(authToken, dataTable)
            return p
        }

        val phoneNumber = CallService.phoneNumberLiveData.observe(this) {}
        Log.d("Response", "response number is $phoneNumber")
        val (token, dataTable) = loadLoginData(this)
        spinner = findViewById(R.id.filter_spinner)
        statusSpinner = findViewById(R.id.status_filter_spinner)
        setupSpinner(token!!, dataTable!!)
        statusSpinner(token!!, dataTable!!)
        loadContacts(currentCategory, currentStatus, token!!, dataTable!!)
        Log.d("observ", "done clear $phoneNumber")
        if (phoneNumber != null) {
            CallService.phoneNumberLiveData.observe(this) { phoneNumber ->
                filterContactsByNumber(token!!, phoneNumber!!, currentCategory, dataTable)
                Log.d("observ", "done  $phoneNumber")
            }
        } else {
            Log.d("Response", "phone number is null")
        }
        searchButton.setOnClickListener {
            val searchableText = searchableEditText.text.toString()
            searchEntrant(token!!, searchableText, dataTable)
        }
        clearButton.setOnClickListener {
            val searchableText = searchableEditText.text.clear()
            setupSpinner(token!!, dataTable!!)
            statusSpinner(token!!, dataTable!!)
            loadContacts(currentCategory, currentStatus, token!!, dataTable!!)
        }

        Log.d("MainActivity", "onCreate() called")
    }

    private fun statusSpinner(token: String, dataTable: String) {
        val statuses = arrayOf("Все", "Согласен", "Думает", "Отказ", "Нет")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, statuses)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        statusSpinner.adapter = adapter

        statusSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentStatus = statuses[position]
                loadContacts(currentCategory, currentStatus, token!!, dataTable!!) // Изменено
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun requestScreeningRole(activity: Activity) {
        val roleManager = activity.getSystemService(Context.ROLE_SERVICE) as RoleManager
        val isHeld = roleManager.isRoleHeld(RoleManager.ROLE_CALL_SCREENING)
        if (!isHeld) {
            val intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_CALL_SCREENING)
            requestPermissionLauncher.launch(intent)
        } else {
            //
        }

    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { }

    private fun setupSpinner(token: String, dataTable: String) {
        val categoriesEntrant = arrayOf("Все", "РПО", "Дизайн") // Список для Entrant
        val categoriesService = arrayOf("Все", "Сервис", "Сервис") // Список для ServiceEntrant

        // Choose array independent of dataTable
        val currentCategories =
            if (dataTable == "EntrantService") categoriesService else categoriesEntrant

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, currentCategories)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                // Using choose array for filter
                currentCategory = currentCategories[position]
                loadContacts(currentCategory, currentStatus, token!!, dataTable!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun loadContacts(category: String, status: String, token: String, dataTable: String) {
        val token = token
        CoroutineScope(Dispatchers.IO).launch {
            try {
                // Загружаем все данные для выбранной категории
                // Сюда же добавляются новые категории для других направлений
                val response = when (category) {
                    "РПО" -> ContactAPIClient.contactReq.getContactsRPO(dataTable, token)
                    "Дизайн" -> ContactAPIClient.contactReq.getContactsDesign(dataTable, token)
                    "тестовое" -> ContactAPIClient.contactReq.getContacts1(dataTable, token)
                    else -> ContactAPIClient.contactReq.getContacts(dataTable, token)
                }

                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val allContacts = response.body()?.list

                        // Фильтруем данные по статусу на стороне клиента
                        val filteredContacts = if (status == "Все") {
                            allContacts // Если статус "Все", то не фильтруем
                        } else {
                            allContacts?.filter { contact ->
                                contact.status == status // Фильтруем по статусу
                            }
                        }

                        // Отображаем отфильтрованные данные
                        if (filteredContacts != null) {
                            recyclerView.apply {
                                layoutManager = LinearLayoutManager(this@MainActivity)
                                adapter = ContactAdapter(filteredContacts, context)
                            }
                        } else {
                            Log.d("Contacts", "Contacts is null")
                        }

                    } else {
                        Log.d("Response", "Response is un Succ")
                    }
                }
            } catch (e: Exception) {
                Log.e("Error", e.message.toString())
            }
        }
    }

    private fun filterContactsByNumber(
        token: String,
        phoneNumber: String,
        category: String,
        dataTable: String
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                if (phoneNumber != null) {
                    val response = ContactAPIClient.contactReq.getContactsByNumber(
                        dataTable,
                        token!!,
                        "snils,comment,priority,speciality,score,phoneNumber,lastName,status,name",
                        "textFilter",
                        phoneNumber
                    )
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            val contacts = response.body()?.list
                            if (contacts != null && contacts.isNotEmpty()) {
                                recyclerView.adapter = ContactAdapter(contacts, this@MainActivity)
                            } else {
                                Log.d("Contacts", "Contacts not found for : $phoneNumber")
                            }
                        } else {
                            Toast.makeText(
                                this@MainActivity,
                                "Пользователя нет в базе",
                                Toast.LENGTH_LONG
                            ).show()
                            Log.d("Response", "Response is unsuccessful")
                        }
                    }
                } else {
                    Log.d("observ", "phoneNumber is null")
                }
            } catch (e: Exception) {
                Log.e("Error", e.message.toString())
            }
        }
    }

    private fun searchEntrant(token: String, inputText: String, dataTable: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ContactAPIClient.contactReq.searchEntrant(
                    dataTable,
                    token!!,
                    "snils,comment,priority,speciality,score,phoneNumber,lastName,status,name",
                    "textFilter",
                    inputText
                )

                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val contacts = response.body()?.list
                        if (contacts != null && contacts.isNotEmpty()) {
                            recyclerView.adapter = ContactAdapter(contacts, this@MainActivity)
                        } else {
                            Log.d("Contacts", "Contacts not found for : $inputText")
                        }
                    } else {
                        Log.d("Response", "Response is unsuccessful")
                    }
                }
            } catch (e: Exception) {
                Log.e("Error", e.message.toString())
            }
        }
    }
}