package com.example.phonecallingtest.login

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.example.phonecallingtest.MainActivity
import com.example.phonecallingtest.R
import com.example.phonecallingtest.login.loginNetwork.LoginAPIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginActivity : AppCompatActivity() {
    private lateinit var loginBtn: Button
    private lateinit var loginText: EditText
    private lateinit var passwordText: EditText

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginBtn = findViewById(R.id.btn_log_in)
        loginText = findViewById(R.id.login)
        passwordText = findViewById(R.id.password)

        fun saveLoginData(context: Context, authToken: String, userGroup: String) {
            val sharedPrefrences = context.getSharedPreferences("LoginData", Context.MODE_PRIVATE)
            with(sharedPrefrences.edit()) {
                putString("authToken", authToken)
                putString("dataTable", userGroup)
                apply()
            }
        }
        loginBtn.setOnClickListener {
            val loginTextEntered = loginText.text.toString()
            val passwordTextEntered = passwordText.text.toString()
            var token = "$loginTextEntered:$passwordTextEntered"
            val authBytes = token.toByteArray(Charsets.UTF_8)
            var group: String = ""
            token = "Basic " + Base64.encodeToString(authBytes, Base64.NO_WRAP)
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = LoginAPIClient.contactReq.getUsers(token)
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            val users = response.body()?.list
                            if (users != null) {
                                for (user in users) {
                                    if (user.defaultTeamName != null) {

                                        group = user.defaultTeamName

                                    } else (Log.d("LoginActivity", user.toString() + " is null"))
                                }
                            }
                            Log.d("LoginActivity", group)
                            saveLoginData(this@LoginActivity, token, group)
                            val intent = Intent(it.context, MainActivity::class.java)
                            intent.putExtra("token", token)
                            startActivity(intent)
                            finish()
                            Log.d("Response", "$response")
                        } else {
                            Log.d("Response", "Response is un Succ")
                            val builder = AlertDialog.Builder(this@LoginActivity)
                            builder.setTitle("Ошибка авторизации")
                            builder.setMessage("Неправильный логин или пароль")
                            builder.setPositiveButton("OK") { dialog, _ ->
                                dialog.dismiss()
                            }
                            builder.show()
                        }
                    }
                } catch (e: Exception) {
                    val builder = AlertDialog.Builder(this@LoginActivity)
                    builder.setTitle("Ошибка авторизации")
                    builder.setMessage("Неправильный логин или пароль")
                    builder.setPositiveButton("OK") { dialog, _ ->
                        dialog.dismiss()
                    }
                    builder.show()
                    Log.e("Error", e.message.toString())
                }
            }

        }

    }

    private fun isAuthorized(context: Context): Boolean {
        val sharedPreferences = context.getSharedPreferences("LoginData", Context.MODE_PRIVATE)
        return sharedPreferences.getString("authToken", null) != null
    }
}