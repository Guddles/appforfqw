package com.example.phonecallingtest.login.loginNetwork


import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface LoginApi {
    @Headers("Content-Type: application/json")
    @GET("api/v1/User")
    suspend fun getUsers(@Header("Authorization") apiKey: String): Response<UserResponse>
}

class LoginAPIClient {
    companion object {
        private val retrofit: Retrofit by lazy {
            with(Retrofit.Builder()) {
                baseUrl("http://84.201.179.32/")
                addConverterFactory(GsonConverterFactory.create())
                build()
            }
        }
        val contactReq: LoginApi by lazy {
            retrofit.create(LoginApi::class.java)
        }

    }
}
