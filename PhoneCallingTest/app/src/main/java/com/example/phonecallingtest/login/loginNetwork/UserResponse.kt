package com.example.phonecallingtest.login.loginNetwork

import java.io.Serializable

data class UserResponse(
    val total: Int,
    val list: List<Users>
)

data class Users(
    val id: String,
    val username: String,
    val defaultTeamName: String,
) : Serializable