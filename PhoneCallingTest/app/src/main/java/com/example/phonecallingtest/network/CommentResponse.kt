package com.example.phonecallingtest.network

import android.icu.text.SimpleDateFormat
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date
import java.util.Locale

data class CommentHistoryResponse(
    val total: Int,
    val list: List<CommentHistoryItem>
)

data class CommentHistoryItem(
    val id: String,
    val data: Data,
    val type: String,
    val number: Int,
    @SerializedName("createdAt") val createdAtString: String,
    @SerializedName("modifiedAt") val modifiedAtString: String,
    @SerializedName("createdByName") val createdByName: String?,
    @SerializedName("modifiedByName") val modifiedByName: String?
) : Serializable {

    data class Data(
        val attributes: Attributes
    )

    data class Attributes(
        val became: Became
    )

    data class Became(
        val comment: String,
        val status: String
    )

    //  Форматирование даты
    fun getCreatedAt(): String {
        return formatDate(createdAtString)
    }

    fun getModifiedAt(): String {
        return formatDate(modifiedAtString)
    }

    // Форматирование даты из "yyyy-MM-dd'T'HH:mm:ss" в "dd.MM.yyyy HH:mm"
    private fun formatDate(dateString: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val outputFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
        val date: Date = inputFormat.parse(dateString) ?: Date()
        return outputFormat.format(date)
    }
}