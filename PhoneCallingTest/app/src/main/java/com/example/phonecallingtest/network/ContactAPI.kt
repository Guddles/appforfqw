package com.example.phonecallingtest.network

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

class ContactAPIClient {
    interface ContactApi {
        @Headers("Content-Type: application/json")
        @GET("api/v1/{tableName}")
        suspend fun getContacts(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String
        ): Response<ContactResponse>

        @GET("api/v1/{tableName}?select=snils,comment,priority,speciality,score,phoneNumber,lastName,status,name&maxSize=200&offset=0&orderBy=createdAt&order=desc&where[0][type]=textFilter&where[0][value]=РПО")
        suspend fun getContactsRPO(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String
        ): Response<ContactResponse>

        @GET("api/v1/{tableName}?select=snils,comment,priority,speciality,score,phoneNumber,lastName,status,name&maxSize=200&offset=0&orderBy=createdAt&order=desc&where[0][type]=textFilter&where[0][value]=тестовое")
        suspend fun getContacts1(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String
        ): Response<ContactResponse>

        @GET("api/v1/{tableName}?select=snils,comment,priority,speciality,score,phoneNumber,status,lastName,name&maxSize=200&offset=0&orderBy=createdAt&order=desc&where[0][type]=textFilter&where[0][value]=Дизайн")
        suspend fun getContactsDesign(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String
        ): Response<ContactResponse>

        @GET("api/v1/{tableName}")
        suspend fun getContactsByNumber(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String,
            @Query("select") select: String,
            @Query("where[0][type]") type: String,
            @Query("where[0][value]") phoneNumber: String
        ): Response<ContactResponse>

        @GET("api/v1/{tableName}")
        suspend fun searchEntrant(
            @Path("tableName") tableName: String,
            @Header("Authorization") apiKey: String,
            @Query("select") select: String,
            @Query("where[0][type]") type: String,
            @Query("where[0][value]") inputText: String
        ): Response<ContactResponse>
    }
    companion object {
        private val retrofit: Retrofit by lazy {
            with(Retrofit.Builder()) {
                baseUrl("http://84.201.179.32/")
                addConverterFactory(GsonConverterFactory.create())
                build()
            }
        }
        val contactReq: ContactApi by lazy {
            retrofit.create(ContactApi::class.java)
        }

    }
}
