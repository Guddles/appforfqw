package com.example.phonecallingtest.network

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.PUT
import retrofit2.http.Path

interface ContactInfoApi {
    @GET("api/v1/{tableName}/{id}")

    fun getContactInfo(
        @Header("Authorization") apiKey: String,
        @Path("id") id: String,
        @Path("tableName") tableName: String,
    ): Call<Contact>

    @PUT("api/v1/{tableName}/{id}")
    fun updateComment(
        @Header("Authorization") apiKey: String,
        @Path("id") id: String,
        @Path("tableName") tableName: String,
        @Body comment: JsonObject
    ): Call<Contact>

    @PUT("api/v1/{tableName}/{id}")
    fun updateStatus(
        @Header("Authorization") apiKey: String,
        @Path("id") id: String,
        @Path("tableName") tableName: String,
        @Body status: JsonObject
    ): Call<Contact>

    @GET("api/v1/{tableName}/{id}/stream")
    fun getCommentHistory(
        @Header("Authorization") apiKey: String,
        @Path("id") id: String,
        @Path("tableName") tableName: String
    ): Call<CommentHistoryResponse>

}

class ContactInfoAPIClient {
    companion object {
        private val retrofit: Retrofit by lazy {
            with(Retrofit.Builder()) {
                baseUrl("http://84.201.179.32/")
                addConverterFactory(GsonConverterFactory.create())
                build()
            }
        }
        val contactReqInfo: ContactInfoApi by lazy {
            retrofit.create(ContactInfoApi::class.java)
        }

    }
}
