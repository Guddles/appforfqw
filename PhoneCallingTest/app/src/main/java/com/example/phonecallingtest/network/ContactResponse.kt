package com.example.phonecallingtest.network

import android.text.Editable
import android.widget.EditText
import java.io.Serializable

data class ContactResponse(
    val total: Int,
    val list: List<Contact>
)

data class Contact(
    val id: String,
    val name: String,
    val lastName: String,
    val phoneNumber: String,
    val parentPhoneNumber: String,
    val speciality: String,
    val score: String,
    val priority: String,
    val snils: String,
    var comment: String,
    val status: String
) : Serializable
